# Timelog

* Rhythmic Tapping for Mobile Device Input
* Andrea Pifkova
* 2403464P
* Euan Freeman 


## Week 2

### 3 Oct 2021

* *4 hours* Read research papers mentioned by supervisor


## Week 3

### 5 Oct 2021

* *1 hour* Initial meeting with supervisor

### 9 Oct 2021

* *3 hours* Read the project guidance 


## Week 4

### 12 Oct 2021

* *0.5 hour* Meeting with supervisor
* *1 hour* Set up GitLab repository and cloned the template for the project

### 17 Oct 2021
* *0.5* Modified timelog template 


## Week 5

### 18 Oct 2021




