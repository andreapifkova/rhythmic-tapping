# Plan

* Rhythmic Tapping for Mobile Device Input
* Andrea Pifkova
* 2403464P
* Euan Freeman


## Winter semester

* **Week 1** Learn about the project 
* **Week 2** Research
* **Week 3** Research 
* **Week 4** Research 
* **Week 5** Research
* **Week 6**
* **Week 7**
* **Week 8**
* **Week 9**
* **Week 10**
* **Week 11 [PROJECT WEEK]**
* **Week 12 [PROJECT WEEK]** Status report submitted.

## Winter break

## Spring Semester

* **Week 13**
* **Week 14**
* **Week 15**
* **Week 16**
* **Week 17**
* **Week 19**
* **Week 20**
* **Week 21**
* **Week 22**
* **Week 23 [TERM ENDS]**
* **Week 24** Dissertation submission deadline and presentations.

