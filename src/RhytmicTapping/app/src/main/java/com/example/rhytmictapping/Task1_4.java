package com.example.rhytmictapping;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class Task1_4 extends AppCompatActivity implements
        View.OnTouchListener,
        Animation.AnimationListener {

    public static final int SLEEP_INTERVAL = 25; // milliseconds TODO find better value
    public static final double CORRELATION_THRESHOLD = 0.3; // Correlation coefficient
    public static final double MINIMUM_DURATION_THRESHOLD = 500; // milliseconds
    public static final double MINIMUM_DISTANCE_THRESHOLD = 100; // pixels?
    public static final int MINIMUM_NUMBER_OF_READINGS = (int) (MINIMUM_DURATION_THRESHOLD / SLEEP_INTERVAL); // TODO find better size?
    public static final int MAXIMUM_NUMBER_OF_READINGS = (int) (3 * MINIMUM_DURATION_THRESHOLD / SLEEP_INTERVAL); // TODO find better size?
    public static final int SPEED = 1800;

    //    public static final String FILENAME = "results_" + System.currentTimeMillis() + ".txt";
    public static final String FILENAME = "results_1.txt";
    // Thread runs the pursuits logic
    MyThread mThread = null;
    boolean moved_enough = false;
    double start_x, start_y;
    // Moving target(s)
    private Task1_2.Target mTarget1;
    private ArrayList<Task1_2.Target> mTargets;
    // Most recent user touch coordinates
    private double userX, userY;
    // Lists for storing user touch positions
    private ArrayList<Double> xInputArrayList, yInputArrayList;
    // Debut output text fields
//    private TextView mTextUserX;
//    private TextView mTextUserY;
//    private TextView mTextCorrelation;
    private TextView mTextStatus;
    private String mStatus;
    private String mCorrelationStatus;
    // Experiment stuff
    private int touchCounter;
    private long touchTime, initTouchTime;
    private long completionTime;
    private String tag = "Experiment ";

    @RequiresApi(api = Build.VERSION_CODES.N)
    static double correlation(ArrayList<Double> x1, ArrayList<Double> x2, ArrayList<Double> y1, ArrayList<Double> y2) {
        if (x1.size() < MINIMUM_NUMBER_OF_READINGS) {
            return 0.0;
        }

        double[] x1A = x1.stream().mapToDouble(Double::doubleValue).toArray();
        double[] x2A = x2.stream().mapToDouble(Double::doubleValue).toArray();
        double[] y1A = y1.stream().mapToDouble(Double::doubleValue).toArray();
        double[] y2A = y2.stream().mapToDouble(Double::doubleValue).toArray();

        @SuppressWarnings("SuspiciousNameCombination")
        double correlationX = new PearsonsCorrelation().correlation(x1A, x2A);

        @SuppressWarnings("SuspiciousNameCombination")
        double correlationY = new PearsonsCorrelation().correlation(y1A, y2A);

        return Math.min(Math.abs(correlationX), Math.abs(correlationY));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task14);

        xInputArrayList = new ArrayList<>(MAXIMUM_NUMBER_OF_READINGS);
        yInputArrayList = new ArrayList<>(MAXIMUM_NUMBER_OF_READINGS);

        mTargets = new ArrayList<>();

        // experiment
        touchCounter = 0;
        touchTime = -1;
        initTouchTime = -1;


        // Moving target view
        final ImageView target1View = findViewById(R.id.imageView); // big circle
//        final ImageView target2View = findViewById(R.id.imageView2);

        mTarget1 = new Task1_2.Target(150, 180, 125, 0, target1View);
//        mTarget2 = new Target(180, 460, 90, 0, target2View);
        mTargets.add(mTarget1);
//        mTargets.add(mTarget2);

        // Diagnostic text fields
        mStatus = "...";
        mTextStatus = findViewById(R.id.textViewSuccess);
        mTextStatus.setText(mStatus);

//        mTextUserX = findViewById(R.id.textViewUserX);
//        mTextUserX.setText("");
//
//        mTextUserY = findViewById(R.id.textViewUserY);
//        mTextUserY.setText("");
//
//        mCorrelationStatus = "N/A";
//        mTextCorrelation = findViewById(R.id.textViewCorrelation);
//        mTextCorrelation.setText(mCorrelationStatus);

        // Rotation animation
        ObjectAnimator rotateAnimation = ObjectAnimator.ofFloat(target1View, "rotation", 360, 0);
        rotateAnimation.setDuration(SPEED);
        rotateAnimation.setRepeatCount(ObjectAnimator.INFINITE);
        rotateAnimation.setInterpolator(new LinearInterpolator());


        // Update the moving target position with the animated value
        rotateAnimation.addUpdateListener(animation -> {
            float value = (float) animation.getAnimatedValue();
            mTarget1.updateAnimation(value, mTarget1.angularOffset);
        });

        rotateAnimation.start();

        // Touch listener
        View screenView = findViewById(R.id.view);
        screenView.setOnTouchListener(this);

        createFile(FILENAME, "__________________________\n" + "_________TASK 1.4_________\n");

    }

    public void nextTask() {
        Intent intent = new Intent(this, Task1_5.class);
        startActivity(intent);
    }

    private void createSnackbar() {
        View wholeView = findViewById(R.id.wholeView);
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Success :)", Snackbar.LENGTH_INDEFINITE)
                .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_FADE)
                .setBackgroundTint(Color.parseColor("#006700"))
                .setAction("NEXT", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nextTask();
                    }
                });
        snackbar.show();
    }

    public void createFile(String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Download/Results");
            if (!root.exists()) {
                root.mkdirs();
            }
            Log.d("TAG", root.getPath()); //<-- check the log to make sure the path is correct.
            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile, true);
            writer.append(sBody);
            writer.flush();
            writer.close();
//            Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private synchronized void updatePositionLists() {
        xInputArrayList.add(userX);
        yInputArrayList.add(userY);

        for (Task1_2.Target target : mTargets) {
            target.updateList();
        }

        // Trim
        if (xInputArrayList.size() >= MAXIMUM_NUMBER_OF_READINGS) {
            xInputArrayList.remove(0);
            yInputArrayList.remove(0);

            for (Task1_2.Target target : mTargets) {
                target.dropListHead();
            }
        }
    }

    private synchronized void clearArrays() {
        for (Task1_2.Target target : mTargets) {
            target.targetXArrayList.clear();
            target.targetYArrayList.clear();
        }

        xInputArrayList.clear();
        yInputArrayList.clear();
    }

    // touch detector methods
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();


        switch (action) {
            case (MotionEvent.ACTION_DOWN):
                clearArrays();
                touchTime = System.currentTimeMillis();
                if (initTouchTime == -1) {
                    initTouchTime = System.currentTimeMillis();
                }
                touchCounter++;
                Log.d(tag, "Touch time: " + touchTime);
                createFile(FILENAME, "Touch time: " + touchTime + "\n");

                moved_enough = false;
                start_x = motionEvent.getX();
                start_y = motionEvent.getY();

                mThread = new MyThread();
                mThread.start();

//                Log.d("TAG", "IM HERE");
//                createFile(FILENAME, "file body xD\n");

                return true;
            case (MotionEvent.ACTION_MOVE):
                userX = motionEvent.getX();
                userY = motionEvent.getY();

//                mTextUserX.setText(String.format(Locale.ENGLISH, "%.0f", userX));
//                mTextUserY.setText(String.format(Locale.ENGLISH, "%.0f", userY));

                if (!moved_enough) {
                    if (Math.abs(start_x - userX) >= MINIMUM_DISTANCE_THRESHOLD) {
                        moved_enough = true;
                    } else if ((Math.abs(start_x - userY) >= MINIMUM_DISTANCE_THRESHOLD)) {
                        moved_enough = true;
                    }
                }

                mTextStatus.setText(mStatus);
//                mTextCorrelation.setText(mCorrelationStatus);

                return true;
            case (MotionEvent.ACTION_UP):
            case (MotionEvent.ACTION_CANCEL):
                mThread.stopThread();

                return true;
            case (MotionEvent.ACTION_OUTSIDE):
                return true;
            default:
                return super.onTouchEvent(motionEvent);
        }
    }

    /**
     * Run the pursuits correlation algorithm to find the best matching driver.
     *
     * @param userX   List of user x coordinates.
     * @param userY   List of user y coordinates.
     * @param targets List of pursuit {@link Task1_2.Target} instances.
     * @return Instance of {@link Task1_2.PursuitResult}.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    Task1_2.PursuitResult pursuits(ArrayList<Double> userX, ArrayList<Double> userY, ArrayList<Task1_2.Target> targets) {

        double bestCorrelation = -1.0;
        Task1_2.Target bestDriver = null;

        for (Task1_2.Target target : targets) {
            double correlation = correlation(userX, target.targetXArrayList, userY, target.targetYArrayList);

            if (correlation > bestCorrelation) {
                bestCorrelation = correlation;
                bestDriver = target;
            }
        }

        return new Task1_2.PursuitResult(bestDriver, bestCorrelation);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    /**
     * This class represents a single circular animation target.
     */

    static class Target {
        int originX;
        int originY;
        int radius;
        int angularOffset;
        ImageView targetView;
        double targetX;
        double targetY;
        ArrayList<Double> targetXArrayList;
        ArrayList<Double> targetYArrayList;

        public Target(int originX, int originY, int radius, int angularOffset, ImageView targetView) {
            this.originX = originX;
            this.originY = originY;
            this.radius = radius;
            this.angularOffset = angularOffset;
            this.targetView = targetView;

            targetXArrayList = new ArrayList<>(MAXIMUM_NUMBER_OF_READINGS);
            targetYArrayList = new ArrayList<>(MAXIMUM_NUMBER_OF_READINGS);
        }

        /**
         * Call this function to calculate the next frame in the animation - this updates
         * the target coordinates (which are used to render) but doesn't update the list
         * of stored target locations.
         */
        public synchronized void updateAnimation(float frame_value, int angularOffset) {
            targetX = originX + radius * Math.cos(Math.toRadians(frame_value + angularOffset));
            targetY = originY + radius * Math.sin(Math.toRadians(frame_value + angularOffset));
        }

        /**
         * Call this function to store updated target positions. This should be done
         * separately (rather than in {@link #updateAnimation}) so that target positions
         * are paired with updated touch positions.
         */
        public synchronized void updateList() {
            targetXArrayList.add(targetX);
            targetYArrayList.add(targetY);
        }

        public void dropListHead() {
            targetXArrayList.remove(0);
            targetYArrayList.remove(0);
        }
    }

    /**
     * This class represents the result of a pursuits calculation.
     */
    static class PursuitResult {
        Task1_2.Target bestMatch;
        double correlation;

        public PursuitResult(Task1_2.Target bestMatch, double correlation) {
            this.bestMatch = bestMatch;
            this.correlation = correlation;
        }
    }

    class MyThread extends Thread {
        boolean keepRunning = true;

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void run() {
            long threshold_exceeded_time = -1;

            while (keepRunning) {
                try {
                    sleep(SLEEP_INTERVAL);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Task1_2.PursuitResult pursuitResult = null;

                // Store our matched pairs of {target, user} coordinates
                updatePositionLists();

                if (moved_enough) {
                    pursuitResult = pursuits(xInputArrayList, yInputArrayList, mTargets);

                    mCorrelationStatus = String.valueOf(pursuitResult.correlation);
                }

                // Set colours of all targets to unmatched - we can set the matched one later
                for (Task1_2.Target target : mTargets) {
                    target.targetView.setColorFilter(Color.argb(100, 255, 0, 0));
                }

                if (pursuitResult == null) {
                    mStatus = "...";
                    mCorrelationStatus = "N/A";
                } else {
                    boolean success = false;

                    if (Math.abs(pursuitResult.correlation) >= CORRELATION_THRESHOLD) {
                        /*
                         * We have exceeded the correlation threshold but want to make sure
                         * sufficient time has passed.
                         */
                        if (threshold_exceeded_time == -1) {
                            threshold_exceeded_time = System.currentTimeMillis();
                        } else {
                            long duration = System.currentTimeMillis() - threshold_exceeded_time;

                            success = duration >= MINIMUM_DURATION_THRESHOLD;
                        }
                    } else {
                        threshold_exceeded_time = -1;
                    }

                    mStatus = success ? "Success!" : "...";
                    mCorrelationStatus = String.format(Locale.ENGLISH, "%.2f", pursuitResult.correlation);
//                    Log.d(tag, "Correlation + " + mCorrelationStatus);

                    // Update colours of best matching target if it exceeds threshold; log counter data
                    if (success) {
                        for (Task1_2.Target target : mTargets) {
                            if (target == pursuitResult.bestMatch) {
                                target.targetView.setColorFilter(Color.argb(100, 0, 255, 0));
                                createSnackbar();
                            }
                        }
                        if (touchCounter != 0) {
                            Log.d(tag, "Tries to succeed: " + touchCounter);
                            createFile(FILENAME, "Tries to succeed: " + touchCounter + "\n");
                            touchCounter = 0;
                        }
                        if (initTouchTime != -1) {
                            completionTime = System.currentTimeMillis() - initTouchTime;
                            Log.d(tag, "Completion time (ms): " + completionTime); //millis
                            createFile(FILENAME, "Completion time (ms): " + completionTime + "\n");
                            initTouchTime = -1;
                        }

                    }
                }
            }
        }

        public void stopThread() {
            keepRunning = false;
        }
    }
}
